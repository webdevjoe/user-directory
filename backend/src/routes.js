const auth = require('./auth')
const errors = require('./errors')

module.exports = function (app) {
  const User = app.get('User')

  app.post('/login', async (req, res, next) => {
    const { email, password } = req.body
    if (!email || !password || password.length < 6 || email.indexOf('@') < 0) {
      return next(new errors.NotAuthenticated('Invalid credentials'))
    }

    // Find users
    const user = await User.scope('login').findOne({ where: { email } })
    if (!user) return next(new errors.NotAuthenticated('Invalid credentials'))

    // Validate password
    const valid = await auth.validatePassword(password, user.password)
    if (!valid) return next(new errors.NotAuthenticated('Invalid credentials'))

    // Generate token
    const token = await auth.generateToken(user)
    res.json({ token })
  })

  app.use(auth.authenticate)
  app.get('/users', async (req, res, next) => {
    const results = await User.findAll()
    res.json(results)
  })
  app.get('/users/:id', async (req, res, next) => {
    const id = req.params.id
    const result = await User.findOne({ where: { id } })
    if (result) {
      res.json(result)
    } else {
      next(new errors.NotFound('No user found with id ' + id))
    }
  })
  app.post('/users', async (req, res, next) => {
    const data = req.body

    if (!data.password) {
      return next(new errors.BadRequest('Missing required fields'))
    }
    data.password = await auth.hashPassword(data.password)
    try {
      const result = await User.create(req.body, { returning: true })

      // Cleanup for return
      res.json({ ...result.toJSON(), password: undefined })
    } catch (err) {
      if (err.message.indexOf('notNull Violation') >= 0) {
        return next(new errors.BadRequest('Missing required fields'))
      } else if (err.message.indexOf('Validation error') >= 0) {
        return next(new errors.BadRequest('Invalid request'))
      } else {
        return next(new errors.ServerError(err.message))
      }
    }
  })
  app.patch('/users/:id', async (req, res, next) => {
    const id = req.params.id
    const data = req.body
    if (data.password) {
      data.password = await auth.hashPassword(data.password)
    }
    try {
      const [rowsUpdated] = await User.update(req.body, { where: { id } })
      if (rowsUpdated !== 1) {
        return next(new errors.NotFound('No user found with id ' + id))
      }
    } catch (err) {
      if (err.message.indexOf('notNull Violation') >= 0) {
        return next(new errors.BadRequest('Cannot set a required field to null'))
      } else if (err.message.indexOf('Validation error') >= 0) {
        return next(new errors.BadRequest('Invalid request'))
      } else {
        return next(new errors.ServerError(err.message))
      }
    }

    const result = await User.findOne({ where: { id } })
    res.json(result)
  })
  app.delete('/users/:id', async (req, res, next) => {
    const id = req.params.id
    const user = await User.findOne({ where: { id } })
    if (!user) {
      return next(new errors.NotFound('No user found with id ' + id))
    }

    try {
      await User.destroy({ where: { id } })
    } catch (err) {
      return next(new errors.ServerError(err.message))
    }
    res.json(user)
  })

  // Error handler
  app.use((err, req, res, next) => {
    if (err.statusCode) {
      res.status(err.statusCode)
      res.json({ error: err.type, message: err.message, statusCode: err.statusCode })
    } else {
      console.log('Error in route', err)
      res.status(500)
      res.json({ error: err.message, statusCode: 500 })
    }
  })
}
