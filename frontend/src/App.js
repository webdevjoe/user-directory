import React, { useEffect, Fragment } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Login from './components/Login'
import { Provider } from 'react-redux'
import store from './store'
import setAuthToken from './utils/setAuthToken'
import { loadUser } from './actions/auth'
import Dashboard from './components/Dashboard'
import Alert from './components/Alert'
import PrivateRoute from './components/PrivateRoute'
if (localStorage.token) {
    setAuthToken(localStorage.token)
}
function App() {
    useEffect(() => {
        store.dispatch(loadUser())
    }, [])
    return (
        <Provider store={store}>
            <Router>
                <Fragment>
                    <Alert></Alert>
                    <Switch>
                        {
                            // <Route exact path='/dashboard' component={Dashboard}></Route>
                        }
                        <PrivateRoute
                            exact
                            component={Dashboard}
                            path='/dashboard'
                        ></PrivateRoute>
                        <Route exact path='/' component={Login}></Route>
                    </Switch>
                </Fragment>
            </Router>
        </Provider>
    )
}

export default App
