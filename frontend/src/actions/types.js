export const USER_LOADED = 'USER_LOADED';
export const AUTH_ERROR = 'AUTH_ERROR';

export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGOUT = 'LOGOUT';
export const LOGIN_FAIL = 'LOGIN_FAIL';

export const USER_LIST_LOADED = 'USER_LIST_LOADED';
export const GET_USER_SUCCESS = 'GET_USER_SUCCESS';
export const USER_FAIL = 'USER_FAIL';
export const USER_UPDATED = 'USER_UPDATED';
export const USER_CREATED = 'USER_CREATED';
export const CREATE_USER_FAIL = 'CREATE_USER_FAIL';
export const LOADING = 'LOADING';
export const SET_ALERT = 'SET_ALERT';
export const REMOVE_ALERT = 'REMOVE_ALERT';




