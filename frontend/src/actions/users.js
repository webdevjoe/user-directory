import {
    USER_LIST_LOADED,
    GET_USER_SUCCESS,
    USER_FAIL,
    USER_CREATED,
    USER_UPDATED,
    LOADING
} from './types'
import axios from 'axios'
import { setAlert } from './alert';

export const loadUserList = () => async dispatch => {
    try {
        const res = await axios.get('/users')
        dispatch({
            type: USER_LIST_LOADED,
            payload: res.data
        })
    } catch (err) {
        console.error(err.message)
        dispatch({
            type: USER_FAIL
        })
        dispatch(setAlert('Not Able To Get User List', 'danger'))
    }
}

export const startLoading = () => dispatch => {
    dispatch({
        type: LOADING
    })
}

export const getUser = id => async dispatch => {
    try {
        const res = await axios.get(`/users/${id}`)
        dispatch({
            type: GET_USER_SUCCESS,
            payload: res.data
        })
    } catch (err) {
        console.error(err.message)
        dispatch({
            type: USER_FAIL
        })
        dispatch(setAlert('Not Able To Get User Info', 'danger'))
    }
}

export const createUser = user => async dispatch => {
    try {
        const config = {
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const res = await axios.post(`/users`, user, config)
        dispatch({
            type: USER_CREATED,
            payload: res.data
        })
        dispatch(setAlert('User Created', 'success'))
    } catch (err) {
        const errors = err.response.data.errors
        if (errors) {
        }
        console.error(err.message)
        dispatch({
            type: USER_FAIL
        })
        dispatch(setAlert('Cannot Create User', 'danger'))
    }
}

export const updateUser = (id, user) => async dispatch => {
    try {
        const config = {
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const res = await axios.patch(`/users/${id}`, user, config)
        dispatch({
            type: USER_UPDATED,
            payload: res.data
        })
        dispatch(setAlert('User Updated', 'success'));
    } catch (err) {
        console.error(err.message)
        dispatch({
            type: USER_FAIL
        })
        dispatch(setAlert('Cannot Update User', 'danger'))
    }
}
