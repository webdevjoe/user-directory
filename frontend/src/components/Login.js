import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Button, TextField } from '@material-ui/core'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'

import { login } from '../actions/auth'

const Login = ({ login, auth: { isAuthenticated } }) => {
    // initialize form data
    const [formData, setFormData] = useState({
        email: '',
        password: ''
    })

    // update form data on user input change
    const onInputChange = e => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }

    // handle form submission
    const submitForm = async e => {
        e.preventDefault()
        const { email, password } = formData
        login(email, password)
    }

    return isAuthenticated ? (
        <Redirect to="/dashboard"></Redirect>
    ) : (
        <form className='form-container' onSubmit={submitForm}>
            <TextField
                onChange={onInputChange}
                variant='outlined'
                margin='normal'
                required
                id='email'
                label='Email Address'
                name='email'
                autoComplete='email'
                autoFocus
            />
            <TextField
                onChange={onInputChange}
                variant='outlined'
                margin='normal'
                required
                name='password'
                label='Password'
                type='password'
                id='password'
                autoComplete='current-password'
            />
            <Button type='submit' variant='contained' color='primary'>
                Login
            </Button>
        </form>
    )
}

Login.propTypes = {
    login: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
    auth: state.auth
})

export default connect(mapStateToProps, {
    login
})(Login)
