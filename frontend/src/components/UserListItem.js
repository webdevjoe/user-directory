import React from 'react'
import PropTypes from 'prop-types'
import { Card, CardContent, Button } from '@material-ui/core'

const UserListItem = ({ user: { firstName, lastName, id, email }, clickCallback }) => {
    return (
        <div>
            <Card className='horizontal-card'>
                <CardContent className='flexie'>
                    <p>
                        <strong>#{id}</strong> {firstName} {lastName}
                    </p>
                    <p>{email}</p>
                </CardContent>
                <div className='button-container'>
                    <Button
                        size='small'
                        variant='contained'
                        color='primary'
                        data-modal={id}
                        onClick={clickCallback}
                    >
                        Edit
                    </Button>
                </div>
            </Card>
        </div>
    )
}

UserListItem.propTypes = {
    user: PropTypes.object.isRequired,
    clickCallback: PropTypes.func.isRequired
}

export default UserListItem
