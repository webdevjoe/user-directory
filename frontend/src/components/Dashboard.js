import PropTypes from 'prop-types'
import React, { useEffect, useState, Fragment } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import {
    loadUserList,
    getUser,
    createUser,
    updateUser,
    startLoading
} from '../actions/users'
import { Button, TextField, Modal, Fade, Backdrop } from '@material-ui/core'

import UserListItem from './UserListItem'
import Spinner from './Spinner/Spinner'

import { logout, loadUser } from '../actions/auth'
const Dashboard = ({
    loadUserList,
    logout,
    getUser,
    createUser,
    updateUser,
    startLoading,
    loadUser,
    auth: { isAuthenticated },
    user: { users, loading, user }
}) => {
    const [create, setCreate] = useState(false)
    const [open, setOpen] = useState(false)
    const [userList, setUserList] = useState(users)
    const [formData, setFormData] = useState({
        title: '',
        firstName: '',
        lastName: '',
        email: '',
        phone: '',
        password: '',
        id: '',
        createdAt: '',
        updatedAt: ''
    })

    useEffect(() => {
        setUserList(users)
    }, [loading, users])

    useEffect(() => {
        setFormData({
            title: loading || !user.title ? '' : user.title,
            firstName: loading || !user.firstName ? '' : user.firstName,
            lastName: loading || !user.lastName ? '' : user.lastName,
            email: loading || !user.email ? '' : user.email,
            phone: loading || !user.phone ? '' : user.phone,
            password: loading || !user.password ? '' : user.password,
            id: loading || !user.id ? '' : user.id,
            createdAt: loading || !user.createdAt ? '' : user.createdAt,
            updatedAt: loading || !user.updatedAt ? '' : user.updatedAt
        })
    }, [loading, getUser])

    useEffect(() => {
        loadUser()
        console.log('trying')
        loadUserList()
    }, [])

    const openModal = e => {
        const id = e.currentTarget.dataset.modal
        getUser(id)
        startLoading()
        setCreate(false)
        setOpen(true)
    }
    const createNewUser = () => {
        setFormData({
            title: '',
            firstName: '',
            lastName: '',
            email: '',
            phone: '',
            password: ''
        })
        setCreate(true)
        setOpen(true)
    }

    const submitForm = e => {
        e.preventDefault()
        // validate form data
        const { firstName, lastName, email, phone, password, id } = formData
        const notEmpty =
            firstName && lastName && email && phone && (create ? password : true)
        if (!notEmpty) {
            alert(
                `Please fill in all form data ${!firstName ? '*First Name* ' : ''}${
                    !lastName ? '*Last Name* ' : ''
                }${!email ? '*Email* ' : ''}${!phone ? '*Phone* ' : ''}${
                    !password ? (create ? '*Password* ' : '') : ''
                }`
            )
            return
        }

        if (create) {
            startLoading()
            createUser(formData)
            setOpen(false)
        } else {
            startLoading()
            const json = {}
            for (let key in user) {
                if (user[key] !== formData[key]) {
                    json[key] = formData[key]
                }
            }
            updateUser(id, json)
            setOpen(false)
        }
    }
    const seachByname = e => {
        const name = e.target.value.toLowerCase()
        // const filteredList =
        setUserList(
            users.filter(user => {
                return (
                    user.firstName.toLowerCase().indexOf(name) !== -1 ||
                    user.lastName.toLowerCase().indexOf(name) !== -1
                )
            })
        )
        console.dir(userList)
    }
    const inputChange = e => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }

    // if (!isAuthenticated) {
    //     console.log('isAuthenticated?')
    //     return <Redirect to='/'></Redirect>
    // }

    return loading ? (
        <Spinner></Spinner>
    ) : (
        <Fragment>
            <div className='container'>
                <Button
                    style={{ marginBottom: 20, marginTop: 40 }}
                    variant='contained'
                    color='secondary'
                    onClick={() => logout()}
                >
                    Logout
                </Button>
                <Button
                    style={{ marginBottom: 20, marginTop: 20 }}
                    variant='contained'
                    color='primary'
                    onClick={createNewUser}
                >
                    Create New User
                </Button>
                <TextField
                    id='standard-basic'
                    label='Search By Name'
                    onChange={seachByname}
                />
                <div className='list-container'>
                    {userList.map(user => {
                        return (
                            <UserListItem
                                key={user.id}
                                user={user}
                                clickCallback={openModal}
                            ></UserListItem>
                        )
                    })}
                </div>
            </div>

            <Modal
                aria-labelledby='transition-modal-title'
                aria-describedby='transition-modal-description'
                // className={classes.modal}
                open={open}
                onClose={() => setOpen(false)}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500
                }}
            >
                <Fade in={open}>
                    <div className='modal-form-container'>
                        <form className='user-form' onSubmit={submitForm}>
                            <TextField
                                name='title'
                                fullWidth
                                placeholder='Title'
                                onChange={inputChange}
                                value={formData ? formData.title : ''}
                            ></TextField>
                            <TextField
                                name='firstName'
                                fullWidth
                                required
                                placeholder='First Name'
                                onChange={inputChange}
                                value={formData ? formData.firstName : ''}
                            ></TextField>

                            <TextField
                                fullWidth
                                name='lastName'
                                required
                                placeholder='Last Name'
                                onChange={inputChange}
                                value={formData ? formData.lastName : ''}
                            ></TextField>
                            <TextField
                                fullWidth
                                required
                                name='email'
                                type='email'
                                placeholder='Email'
                                onChange={inputChange}
                                value={formData ? formData.email : ''}
                            ></TextField>
                            <TextField
                                name='phone'
                                fullWidth
                                required
                                placeholder='Phone'
                                onChange={inputChange}
                                value={formData ? formData.phone : ''}
                            ></TextField>

                            {create ? (
                                <Fragment>
                                    <TextField
                                        name='password'
                                        placeholder='Password'
                                        type='password'
                                        onChange={inputChange}
                                        fullWidth
                                        required
                                        value={formData ? formData.password : ''}
                                    ></TextField>
                                </Fragment>
                            ) : (
                                <Fragment>
                                    <TextField
                                        placeholder='ID'
                                        fullWidth
                                        disabled
                                        value={formData ? formData.id : ''}
                                    ></TextField>
                                    <TextField
                                        placeholder='Created At'
                                        fullWidth
                                        disabled
                                        value={formData ? formData.createdAt : ''}
                                    ></TextField>
                                    <TextField
                                        placeholder='Updated At'
                                        fullWidth
                                        disabled
                                        value={formData ? formData.updatedAt : ''}
                                    ></TextField>
                                </Fragment>
                            )}

                            <Button
                                type='submit'
                                style={{ marginTop: 40 }}
                                fullWidth
                                size='small'
                                variant='contained'
                                color='primary'
                            >
                                Submit
                            </Button>
                        </form>
                    </div>
                </Fade>
            </Modal>
        </Fragment>
    )
}

Dashboard.propTypes = {
    auth: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    getUser: PropTypes.func.isRequired,
    loadUserList: PropTypes.func.isRequired,
    createUser: PropTypes.func.isRequired,
    updateUser: PropTypes.func.isRequired,
    startLoading: PropTypes.func.isRequired,
    logout: PropTypes.func.isRequired,
    loadUser: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
    auth: state.auth,
    user: state.user
})

export default connect(mapStateToProps, {
    loadUserList,
    getUser,
    createUser,
    updateUser,
    startLoading,
    loadUser,
    logout
})(Dashboard)
