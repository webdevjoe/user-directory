import {
    USER_LIST_LOADED,
    USER_FAIL,
    GET_USER_SUCCESS,
    USER_CREATED,
    CREATE_USER_FAIL,
    LOADING,
    USER_UPDATED
} from '../actions/types'

const initialState = {
    users: [],
    user: { title: '' },
    loading: true,
    error: false
}

export default function(state = initialState, action) {
    const { type, payload } = action
    switch (type) {
        case LOADING:
            return {
                ...state,
                loading: true
            }
        case USER_LIST_LOADED:
            return {
                ...state,
                users: payload,
                loading: false
            }
        case GET_USER_SUCCESS:
            return {
                ...state,
                user: payload,
                loading: false
            }
        case USER_CREATED:
            return {
                ...state,
                loading: false,
                users: [...state.users, payload]
            }
        case USER_UPDATED:
            return {
                ...state,
                loading: false,
                users: [payload, ...state.users.filter(user => user.id !== payload.id)]
            }
        case USER_FAIL:
        case CREATE_USER_FAIL:
            return {
                ...state,
                error: true,
                loading: false
            }
        default:
            return state
    }
}
